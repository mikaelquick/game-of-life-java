import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.*;
import java.io.*;

public class Gui extends JFrame {

    Board grid = new Board();

    private final int screenX = 1024;
    private final int screenY = 768;
    boolean inCorrect = true;

    //Knappar , Sliders , Labels
    JToggleButton startButtom = new JToggleButton("Start");
    JToggleButton pausButtom = new JToggleButton("Paus");
    JButton saveButtom = new JButton("Save");
    JButton exitButtom = new JButton("Exit");
    JSlider speedSlider = new JSlider();
    JLabel speedSliderText = new JLabel("Speed");


    //Menyer
    JMenuBar menuBar = new JMenuBar();
    JMenu menu = new JMenu("Cells");
    JMenuItem randomMenu = new JMenuItem("Randomize");
    JMenuItem gliderMenu = new JMenuItem("Glider");
    JMenuItem exploderMenu = new JMenuItem("Exploder");

    public Gui() throws InterruptedException {

        //Kollar om filen settings finns och frågar om användaren vill ladda filen.
        File file = new File("Settings.txt");
        if (file.exists()) {
            int wantToLoadFile = JOptionPane.showConfirmDialog(null, "Load the saved file?", "Game of Life", JOptionPane.YES_NO_OPTION);

            try {
                if (wantToLoadFile == 0) {
                    FileReader filereader = new FileReader("Settings.txt");
                    BufferedReader buffRead = new BufferedReader(filereader);

                    //Tillsätter värden
                    grid.gridX = Integer.parseInt(buffRead.readLine());
                    grid.gridY = Integer.parseInt(buffRead.readLine());
                    grid.xplace = Integer.parseInt(buffRead.readLine());
                    grid.yplace = Integer.parseInt(buffRead.readLine());
                    Cell.xSize = Integer.parseInt(buffRead.readLine());
                    Cell.ySize = Integer.parseInt(buffRead.readLine());

                    //Stänger filen
                    filereader.close();
                    buffRead.close();

                    //Skapar celler , Läser in objektet och skriver in värden till cells.
                    grid.createCells();
                    grid.loadObject("Array.txt");
                    grid.showPanel();
                }
                else {
                    sizeManager();
                }
            } catch (IOException e) {
            }

        } else {
            sizeManager();
        }

        //Frame Settings
        this.setLayout(null);
        this.setUndecorated(false);
        this.setResizable(false);
        this.setSize(screenX, screenY);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Game of Life");
        this.setLocationRelativeTo(null);

        //Sliders & Text
        speedSlider.setMinorTickSpacing(10);
        speedSlider.setBounds(50, 575, 150, 20);
        speedSlider.setInverted(true);
        speedSlider.setMinimum(0);
        speedSlider.setMaximum(100);
        speedSliderText.setBounds(105, 555, 150, 20);

        //Meny
        menu.add(randomMenu);
        menu.add(gliderMenu);
        menu.add(exploderMenu);
        menuBar.add(menu);

        //Knappar
        saveButtom.setBounds(850, 620, 80, 30);
        exitButtom.setBounds(245, 620, 80, 30);
        startButtom.setBounds(60, 620, 80, 30);
        pausButtom.setBounds(150, 620, 80, 30);

        //Addar alla knappar,menyer,texter och sliders till framen.
        this.add(saveButtom);
        this.add(exitButtom);
        this.add(speedSlider);
        this.setJMenuBar(menuBar);
        this.add(grid);
        this.add(pausButtom);
        this.add(startButtom);
        this.add(speedSliderText);
        this.setVisible(true);

        //Sparaknapp
        saveButtom.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == saveButtom) {
                    try {
                        FileWriter fileWriter = new FileWriter("Settings.txt");
                        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

                        //Skriver
                        bufferedWriter.write(grid.gridX + "");
                        bufferedWriter.newLine();
                        bufferedWriter.write(grid.gridY + "");
                        bufferedWriter.newLine();
                        bufferedWriter.write(grid.xplace + "");
                        bufferedWriter.newLine();
                        bufferedWriter.write(grid.yplace + "");
                        bufferedWriter.newLine();
                        bufferedWriter.write(Cell.xSize + "");
                        bufferedWriter.newLine();
                        bufferedWriter.write(Cell.ySize + "");

                        //Array
                        FileOutputStream fileOut = new FileOutputStream("Array.txt");
                        ObjectOutputStream objOut = new ObjectOutputStream(fileOut);
                        objOut.writeObject(grid.cells);

                        //Stanger
                        bufferedWriter.flush();
                        bufferedWriter.close();

                        objOut.flush();
                        objOut.close();

                        JOptionPane.showMessageDialog(null, "Settings saved");


                    } catch (IOException e1) {
                        JOptionPane.showMessageDialog(null, "Settings wasn't saved");
                    }
                }
            }
        });

        //Avslutaknapp
        exitButtom.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == exitButtom) {
                    System.exit(0);
                }
            }
        });

        //Startknappp
        startButtom.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    startButtom.setText("Stop");

                    Thread thread = new Thread() {
                        public void run() {
                            try {
                                grid.gameIsOn = true;
                                grid.run();

                            } catch (InterruptedException e1) {
                                e1.printStackTrace();
                            }
                        }
                    };
                    thread.start();
                } else {
                    grid.gameIsOn = false;
                    startButtom.setText("Start");
                    grid.killAll();
                    pausButtom.setText("Paus");
                    repaint();
                }
            }
        });

        //Pausknapp
        pausButtom.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e1) {

                if (e1.getStateChange() == ItemEvent.SELECTED) {
                    pausButtom.setText("Continue");
                    grid.gameIsOn = false;
                } else {
                    pausButtom.setText("Paus");
                    Thread thread = new Thread() {
                        public void run() {
                            try {
                                grid.gameIsOn = true;
                                grid.run();

                            } catch (InterruptedException e1) {
                                e1.printStackTrace();
                            }
                        }
                    };
                    thread.start();
                }

            }
        });

        //Hastighetsslider
        speedSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                grid.speed = ((JSlider) e.getSource()).getValue();
            }
        });

        //Slumpmenu
        randomMenu.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == randomMenu) {
                    try {
                        String sAntalRanCells = JOptionPane.showInputDialog("Number of cells to randomize?");
                        int antalRanCells = Integer.parseInt(sAntalRanCells);
                        grid.giveRandomCellLife(antalRanCells);
                        repaint();
                    } catch (NumberFormatException e1) {
                        JOptionPane.showMessageDialog(null, "NumberFormat error");
                    }
                }
            }
        });

        //Glidermenu
        gliderMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == gliderMenu) {
                    if(grid.gridX >= 4 && grid.gridY >= 4) {
                        grid.killAll();
                        grid.cells[grid.gridX / 2][grid.gridY / 2].isAlive = true;
                        grid.cells[grid.gridX / 2][grid.gridY / 2 - 1].isAlive = true;
                        grid.cells[grid.gridX / 2][grid.gridY / 2 - 2].isAlive = true;
                        grid.cells[grid.gridX / 2 - 1][grid.gridY / 2].isAlive = true;
                        grid.cells[grid.gridX / 2 - 2][grid.gridY / 2 - 1].isAlive = true;
                        repaint();
                    }
                    else{
                        JOptionPane.showMessageDialog(null, "A glider require X and Y to be greater or equal to 4 ");
                    }
                }
            }
        });

        //Explodermenu
        exploderMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == exploderMenu) {
                    if(grid.gridX >= 10 && grid.gridY >= 10) {
                        grid.killAll();
                        grid.cells[grid.gridX / 2][grid.gridY / 2].isAlive = true;
                        grid.cells[grid.gridX / 2][grid.gridY / 2 - 4].isAlive = true;

                        grid.cells[grid.gridX / 2 + 2][grid.gridY / 2].isAlive = true;
                        grid.cells[grid.gridX / 2 + 2][grid.gridY / 2 - 1].isAlive = true;
                        grid.cells[grid.gridX / 2 + 2][grid.gridY / 2 - 2].isAlive = true;
                        grid.cells[grid.gridX / 2 + 2][grid.gridY / 2 - 3].isAlive = true;
                        grid.cells[grid.gridX / 2 + 2][grid.gridY / 2 - 4].isAlive = true;

                        grid.cells[grid.gridX / 2 - 2][grid.gridY / 2].isAlive = true;
                        grid.cells[grid.gridX / 2 - 2][grid.gridY / 2 - 1].isAlive = true;
                        grid.cells[grid.gridX / 2 - 2][grid.gridY / 2 - 2].isAlive = true;
                        grid.cells[grid.gridX / 2 - 2][grid.gridY / 2 - 3].isAlive = true;
                        grid.cells[grid.gridX / 2 - 2][grid.gridY / 2 - 4].isAlive = true;
                        repaint();
                    }
                    else{
                        JOptionPane.showMessageDialog(null, "A exploder need X and Y to be greater or equal to 10");
                    }
                }
            }
        });

        //Mousesettings
        grid.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
            }

            @Override
            public void mousePressed(MouseEvent e) {
                if (e.getX() >= 0 && e.getX() < grid.gridX * Cell.xSize && e.getY() >= 0 && e.getY() < (grid.gridY * Cell.ySize)) {
                        if (grid.cells[e.getX() / Cell.xSize][e.getY() / Cell.ySize].isAlive) {
                            grid.cells[e.getX() / Cell.xSize][e.getY() / Cell.ySize].isAlive = false;
                            repaint();
                        }
                        else{
                            grid.cells[e.getX() / Cell.xSize][e.getY() / Cell.ySize].isAlive = true;
                            repaint();
                        }
                }

            }
        });
        grid.addMouseMotionListener(new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {
                if (e.getX() >= 0 && e.getX() < grid.gridX * Cell.xSize && e.getY() >= 0 && e.getY() < (grid.gridY * Cell.ySize)) {
                    grid.cells[e.getX() / Cell.xSize][e.getY() / Cell.ySize].isAlive = true;
                    repaint();
                }

            }

            @Override
            public void mouseMoved(MouseEvent e) {

            }
        });
    }
    //Sizemanager som låter användaren att ange storlek på spelplanen och även ladda en sparad spelplan.
    public void sizeManager() {
        //Olika storlekar av spelplanen.
        String[] alternativ = {"Smal (20*20)", "Big (100*60)", "Own size"};
        String storlek = (String) JOptionPane.showInputDialog(null, "Select the size of the field", "Sizemanager", JOptionPane.QUESTION_MESSAGE, null, alternativ, alternativ[1]);
        if (storlek == null) {
            System.exit(0);
        }

        if (storlek.equals("Smal (20*20)")) {
            inCorrect = false;
            grid.gridX = 20;
            grid.gridY = 20;
            Cell.xSize = screenX / (grid.gridX);
            Cell.ySize = (550) / (grid.gridY);

        }
        if (storlek.equals("Big (100*60)")) {
            inCorrect = false;
            grid.xplace = 8;
            grid.yplace = 0;
            grid.gridX = 100;
            grid.gridY = 60;
            Cell.xSize = screenX / (grid.gridX);
            Cell.ySize = (550) / (grid.gridY);

        }

        while (inCorrect) {
            if (storlek.equals("Own size")) {
                try {
                    String inputX = JOptionPane.showInputDialog("Number of cells in the X direction");
                    if (inputX == null) {
                        System.exit(0);
                    }

                    String inputY = JOptionPane.showInputDialog("Number of cells in the Y direction");
                    if (inputY == null) {
                        System.exit(0);
                    }

                        grid.gridX = Integer.parseInt(inputX);
                        grid.gridY = Integer.parseInt(inputY);
                        Cell.xSize = screenX / (grid.gridX);
                        Cell.ySize = (550) / (grid.gridY);

                        if (Cell.xSize <= 0 || Cell.ySize <= 0){
                            JOptionPane.showMessageDialog(null, "The number you wrote was so high which made the size of the cell become equal to 0.\nWhich I cant allow");
                        }

                        else {

                            grid.xplace = 8; //(1024 - (grid.gridX * Cell.xSize)) / 2;
                            grid.yplace = 0;
                            inCorrect = false;
                        }

                } catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(null, "Please enter the Y and X with numbers");
                }catch (ArithmeticException e) {
                    JOptionPane.showMessageDialog(null, "Please write a number higher then 0");
                }
            }
        }
        grid.createCells();
        grid.showPanel();
    }
}


