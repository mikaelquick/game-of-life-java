import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.Random;

public class Board extends JPanel {

   Cell cells[][];
   Cell nextGen[][];

    public int yplace;
    public int xplace;
    public int gridX;
    public int gridY;

    int speed = 50;
    boolean gameIsOn = true;

    public void createCells() {
        cells = new Cell[gridX][gridY];
        nextGen = new Cell[gridX][gridY];

        for (int x = 0; x < gridX; x++) {
            for (int y = 0; y < gridY; y++) {
                cells[x][y] = new Cell();
                nextGen[x][y] = new Cell();
            }
        }
    }
    public void showPanel() {
        this.setBounds(xplace, yplace, gridX * Cell.xSize, gridY * Cell.ySize);
    }
    public void loadObject(String objekt){
        try {
            //Input
            FileInputStream fis = new FileInputStream(objekt);
            ObjectInputStream ois = new ObjectInputStream(fis);
            cells = (Cell[][]) ois.readObject();

            //Output
            FileOutputStream fout = new FileOutputStream(objekt);
            ObjectOutputStream oss = new ObjectOutputStream(fout);
            oss.writeObject(cells);

            //Closing
            oss.flush();
            oss.close();

        }catch (FileNotFoundException e){
            JOptionPane.showMessageDialog(null, "File not found");
        }
        catch (IOException e) {
        }
        catch (ClassNotFoundException e){
            JOptionPane.showMessageDialog(null, "Class not found");

        }
    }

    public void paint(Graphics g) {

        for (int x = 0; gridX > x; x++) {
            for (int y = 0; gridY > y; y++) {
                if (cells[x][y].isAlive) {
                    g.setColor(Color.BLACK);
                } else
                    g.setColor(Color.magenta);

                g.fillRect(x * Cell.xSize, y * Cell.ySize, Cell.xSize, Cell.ySize);
            }
        }
    }

    public void run() throws InterruptedException {

        while (gameIsOn) {
            Thread.sleep(speed);
            nextTurn();
            repaint();
        }
    }

    public void giveCellLife(int x, int y) {
        cells[x][y].isAlive = true;
    }

    public void killAll() {
        for (int x = 0; x < gridX; x++){
            for (int y = 0; y < gridY; y++) {
                cells[x][y].isAlive = false;
            }
        }
    }

    public void giveRandomCellLife(int antal) {
        Random ran = new Random();
        if (antal > (gridX * gridY)) {
            JOptionPane.showMessageDialog(null, "The grid is " + gridX * gridY + ".\nAnd you wanted to give " + antal + " Cells life.");
        } else {

            for (int i = 0; i < antal && i < gridX * gridY; i++) {
                giveCellLife(ran.nextInt(gridX), ran.nextInt(gridY));
            }
        }
    }

    public void nextTurn() throws InterruptedException {

        for (int x = 0; x < gridX; x++) {
            for (int y = 0; y < gridY; y++) {

                int neighbors = 0;
                //Undersoker om de narliggande cellerna lever

                //leftisalive
                if ( 0 < x && cells[x - 1][y].isAlive) neighbors++;

                //rightIsalive
                if (x < cells.length-1 && cells[x+1][y].isAlive) neighbors++;

                //upIsalive
                if ( 0 < y && cells[x][y-1].isAlive) neighbors++;

                //downIsalive
                if ( y < cells[x].length-1 && cells[x][y+1].isAlive) neighbors++;

                //upRightIsAlive
                if ( 0 < y && cells.length-1 > x && cells[x+1][y-1].isAlive) neighbors++;

                //downRightIsAlive
                if ( cells[x].length-1 > y && cells.length-1 > x && cells[x+1][y+1].isAlive) neighbors++;

                //upLeftIsAlive
                if ( 0 < x && y > 0 && cells[x-1][y-1].isAlive) neighbors++;

                //downLeftIsAlive
                if ( 0 < x && y < cells[x].length-1 && cells[x-1][y+1].isAlive) neighbors++;

                //Vilkor for survival
                //Any live cell with fewer than two live neighbours dies, as if caused by under-population.
                if (neighbors < 2) nextGen[x][y].isAlive = false;

                //Any live cell with two or three live neighbours lives on to the next generation.
                if (cells[x][y].isAlive && neighbors == 2 || neighbors == 3) nextGen[x][y].isAlive = true;

                //Any dead cell with exactly three live neighbours becomes grid live cell, as if by reproduction.
                if (neighbors == 3 && !cells[x][y].isAlive) nextGen[x][y].isAlive = true;

                //Any live cell with more than three live neighbours dies, as if by over-population.
                if (neighbors > 3 && cells[x][y].isAlive) nextGen[x][y].isAlive = false;
            }
        }
        //Tar värdena från nextGen dom stoppar dem i cells
        for (int x = 0; x < cells.length; x++) {
            for (int y = 0; y < cells[x].length; y++) {
                cells[x][y].isAlive = nextGen[x][y].isAlive;
            }
        }
    }
}
